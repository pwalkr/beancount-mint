"""Test module for Mint beancount importer"""

from tempfile import mkstemp
import os
import unittest

from beancount.ingest import regression_pytest
# pylint: disable=no-name-in-module  # It's definitely there
from __init__ import Mint


account_map = {
    'Bank of America Checking': 'Assets:Bank-of-America-Checking',
    'Citibank': 'Liabilities:Citibank',
    'Discover': 'Liabilities:Discover'}


def translate_transaction(transaction):
    """Returns full source and target accounts"""
    if 'Hidden' in transaction['Labels']:
        return None

    if transaction['Category'] == 'Tithe':
        account_target = 'Expenses:Gifts & Donations:Tithe'
    elif transaction['Category'] == 'Credit Card Payment':
        account_target = 'Liabilities:' + transaction['Description']
        transaction['Description'] = ''
    else:
        account_target = Mint.category(transaction['Category'])

    return account_target


importer = Mint(account_map, translate_transaction,
                join_tag='Split')
testdir = os.path.dirname(__file__) + '/mint_test.d'


@regression_pytest.with_importer(importer)
@regression_pytest.with_testdir(testdir)
class TestMint(regression_pytest.ImporterTestBase):
    """Test importer using beancount ingest regression testing"""


# pylint: disable=too-few-public-methods
class TestOutputBeanfile():
    """Test that importer output passes bean-check"""
    @staticmethod
    def test_check():
        """Test that bean-check successfully compiles output"""
        tfd, tpath = mkstemp()
        try:
            with os.fdopen(tfd, 'w') as outfile:
                with open(testdir + '/mint.csv.extract') as extract:
                    for line in extract:
                        outfile.write(line)
                outfile.write("""
2020-01-01 open Assets:Bank-of-America-Checking USD
2020-01-01 open Liabilities:Discover USD
2020-01-01 open Liabilities:Citibank USD
2020-01-01 open Income:Paycheck USD
2020-01-01 open Expenses:Food-Dining:Alcohol-Bars USD
2020-01-01 open Expenses:Food-Dining:Groceries USD
2020-01-01 open Expenses:Gifts-Donations:Tithe USD
2020-01-01 open Expenses:Home:Home-Supplies USD
    """)
            assert os.system('bean-check "{}"'.format(tpath)) == 0
        finally:
            os.unlink(tpath)


if __name__ == '__main__':
    unittest.main()
