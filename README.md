# Beancount importer for Mint CSV export files

[Beancount](http://furius.ca/beancount/) is a text-file-based double-entry accounting language and system.
This project is an [importer](https://beancount.github.io/docs/importing_external_data.html) for [Mint](https://www.mint.com/) CSV exports.

## Setup

Clone to your `importers` directory, sample folder structure:

    .
    |-- config.py
    |-- importers
    |   |-- __init__.py
    |   `-- mint      # This repository
    |       |-- __init__.py
    `-- personal.beancount

Import the importer and add to your `CONFIG` for beancount import config:

    from importers.mint import Mint

    translate_transaction(txn):
        """Given CSV transformed into dict, return account_source and account_target"""
        return 'Assets:' + txn['Account Name'], Mint.category(txn['Category'])

    CONFIG = [
        Mint(translate_transaction,
             # Tag mint transactions with 'Split' to have them auto-re-joined
             join_tag='Split')
    ]

## Usage

Use with standard `bean-identify`, `bean-extract`, `bean-file` commands, e.g.

    export PYTHONPATH=.
    bean-identify config.py ~/Downloads[/mint.csv]
    bean-extract  config.py ~/Downloads[/mint.csv]
    bean-file     config.py ~/Downloads[/mint.csv] -o file/

## Test

To run tests, execute from inside the importer directory:

    PYTHONPATH=. pytest

To [re]generate importer extracts

    PYTHONPATH=. pytest --generate
