"""A beancount importer for Mint export files"""

import datetime
from decimal import Decimal
import re
from csv import DictReader
from os import path

from beancount.core import data
from beancount.core.amount import Amount, add
from beancount.ingest.importer import ImporterProtocol


# pylint: disable=too-many-instance-attributes
class Mint(ImporterProtocol):
    """A beancount importer for Mint CSV files"""

    # Mint doesn't support international banks at this time
    currency = 'USD'

    def __init__(self, account_map, input_translator, join_tag=None):
        self.account_map = account_map
        self.input_translator = input_translator
        self.join_tag = join_tag

    def identify(self, file):
        return re.match(r".*mint.*\.csv", path.basename(file.name), re.IGNORECASE)

    def file_name(self, file):
        return 'mint.csv'

    def file_account(self, _):
        return 'Mint'

    def file_date(self, file):
        date = datetime.date(2000, 1, 1)
        with open(file.name) as iterable:
            reader = DictReader(iterable)
            for transaction in reader:
                self.transform_csv(transaction)
                if transaction['Date'] > date:
                    date = transaction['Date']
        return date

    def extract(self, file, existing_entries=None):
        meta_index = 0
        entries = []
        with open(file.name) as iterable:
            reader = DictReader(iterable)
            for transaction in reader:
                self.transform_csv(transaction)
                account_source = self.account_map[transaction['Account Name']]
                account_target = self.input_translator(transaction)
                if account_target is None:
                    continue
                account_target = self.sanitize_account(account_target)
                amount = Amount(transaction['Amount'], self.currency)
                if transaction['Transaction Type'] == 'credit':
                    postings = [
                        data.Posting(account_target, -amount, None, None, None, None),
                        data.Posting(account_source, amount, None, None, None,
                                     {'memo': transaction['Original Description']})]
                elif transaction['Transaction Type'] == 'debit':
                    postings = [
                        data.Posting(account_source, -amount, None, None, None,
                                     {'memo': transaction['Original Description']}),
                        data.Posting(account_target, amount, None, None, None, None)]
                else:
                    raise Exception('Unrecognized Transaction Type "{}"'
                                    .format(transaction['Transaction Type']))
                # data.Transaction is callable, this is how the beancount example works
                # pylint: disable=not-callable
                joinable = data.Transaction(
                    {'filename': file.name, 'lineno': meta_index},
                    transaction['Date'],
                    self.FLAG,  # flag
                    transaction['Description'],  # payee ("merchant" in Mint mobile app)
                    transaction['Notes'],  # narration
                    transaction['Labels'],  # tags
                    data.EMPTY_SET,  # links
                    postings)
                join_target = self.find_join_target(joinable, entries)
                if join_target is not None:
                    entries.remove(join_target)
                    entries.append(self.join_transactions(account_source, joinable, join_target))
                else:
                    entries.append(joinable)
                    meta_index += 1
        if self.join_tag is not None:
            for entry in entries:
                entry.tags.discard(self.join_tag)
        return entries

    def find_join_target(self, transaction, entries):
        """Find existing transaction to merge with input"""
        if self.join_tag in transaction.tags:
            for entry in entries:
                if self.join_tag in entry.tags and entry.date == transaction.date:
                    return entry
        return None

    @staticmethod
    def join_transactions(account_source, incoming, existing):
        """Join two split transactions"""
        # Create new transaction - move narration to notes meta (we'll have more than one)
        if existing.narration:
            # data.Transaction is callable, this is how the beancount example works
            # pylint: disable=not-callable
            combined = data.Transaction(existing.meta, existing.date,
                                        existing.flag, existing.payee,
                                        None,  # narration
                                        existing.tags, existing.links,
                                        existing.postings)
            for epost in existing.postings:
                # attach notes to target account posting
                if epost.account != account_source:
                    tpost = epost
                    break
            existing.postings.remove(tpost)
            existing.postings.append(data.Posting(tpost.account,
                                                  tpost.units, None, None, None,
                                                  {'notes': existing.narration}))
        else:
            combined = existing

        for ipost in incoming.postings:
            if ipost.account == account_source:
                # Combine amounts from source account
                tpost = None
                for cpost in combined.postings:
                    if ipost.account == cpost.account:
                        tpost = cpost
                        break
                combined.postings.remove(tpost)
                combined.postings.append(data.Posting(
                    tpost.account,
                    add(tpost.units, ipost.units),
                    None, None, None,
                    tpost.meta))
            else:
                combined.postings.append(data.Posting(ipost.account, ipost.units,
                                                      None, None, None,
                                                      {'notes': incoming.narration}))

        def get_amount(posting):
            return posting.units

        combined.postings.sort(key=get_amount)
        return combined

    @staticmethod
    def transform_csv(transaction):
        """Parse parameters in a csv dict"""
        transaction['Amount'] = Decimal(transaction['Amount'])
        transaction['Date'] = datetime.datetime.strptime(transaction['Date'], '%m/%d/%Y').date()
        transaction['Labels'] = set(transaction['Labels'].split())

    @staticmethod
    def sanitize_account(account):
        """Return account with non-account characters replaced by dash"""
        return re.sub('[^A-Za-z:-_]+', '-', account)

    @staticmethod
    def category(category):
        """Find full path to category based on income and expense category maps"""
        if category == 'Income':
            return 'Income'
        if category in Mint.income_categories:
            return 'Income:' + category
        for parent in Mint.expense_categories:
            if parent == category:
                return 'Expenses:' + category
            if category in Mint.expense_categories[parent]:
                return 'Expenses:{}:{}'.format(parent, category)
        raise Exception('Unrecognized category "{}". Please handle this in input_translator'
                        .format(category))

    account_delimiter = ':'

    income_categories = [
        'Bonus',
        'Interest Income',
        'Paycheck',
        'Reimbursement',
        'Rental Income',
        'Returned Purchase']

    expense_categories = {
        'Auto & Transport': [
            'Auto Insurance',
            'Auto Payment',
            'Gas & Fuel',
            'Parking',
            'Public Transportation',
            'Service & Parts'],
        'Bills & Utilities': [
            'Home Phone',
            'Internet',
            'Mobile Phone',
            'Television',
            'Utilities'],
        'Entertainment': [
            'Amusement',
            'Arts',
            'Music',
            'Movies & DVDs',
            'Music',
            'Newspapers & Magazines'],
        'Fees & Charges': [
            'ATM Fee',
            'Bank Fee',
            'Finance Charge',
            'Late Fee',
            'Service Fee',
            'Trade Commissions'],
        'Financial': [
            'Financial Advisor',
            'Life Insurance',
            'Umbrella Insurance'],
        'Food & Dining': [
            'Alcohol & Bars',
            'Coffee Shops',
            'Fast Food',
            'Groceries',
            'Restaurants'],
        'Gifts & Donations': [
            'Charity',
            'Gift'],
        'Health & Fitness': [
            'Dentist',
            'Doctor',
            'Eyecare',
            'Gym',
            'Health Insurance',
            'Pharmacy',
            'Sports'],
        'Hide from Budgets & Trends': [],
        'Home': [
            'Furnishings',
            'Home Improvement',
            'Home Insurance',
            'Home Services',
            'Home Supplies',
            'Lawn & Garden',
            'Mortgage & Rent'],
        'Kids': [
            'Allowance',
            'Baby Supplies',
            'Babysitter & Daycare',
            'Child Support',
            'Kids Activities',
            'Toys'],
        'Loans': [
            'Loan Fees and Charges',
            'Loan Insurance',
            'Loan Interest',
            'Loan Payment',
            'Loan Principal'],
        'Misc Expenses': [],
        'Personal Care': [
            'Hair',
            'Laundry',
            'Spa & Massage'],
        'Pets': [
            'Daycare & Boarding',
            'Pet Food & Supplies',
            'Pet Grooming',
            'Veterinary'],
        'Shopping': [
            'Books',
            'Clothing',
            'Electronics & Software',
            'Hobbies',
            'Sporting Goods'],
        'Taxes': [
            'Federal Tax',
            'Local Tax',
            'Property Tax',
            'Sales Tax',
            'State Tax'],
        'Travel': [
            'Air Travel',
            'Hotel',
            'Rental Car & Taxi',
            'Vacation'],
        'Uncategorized': [
            'Cash & ATM',
            'Check'],
    }
